package utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendEMail {
	
	private String credentialUser = null;
	private String credentialSecret = null;
	//private final InternetAddress[] toEMails = InternetAddress.parse("jedihonest@gmail.com, valentinalukash@gmail.com", false); //Several e-mails can be in one pair of quotes
	
	/**
	 * This class is used to send e-mail.
	 * 
	 * @param token - file with e-mail credentials.
	 * @param numberOfRooms
	 * @param address
	 * @param price
	 * @param addressURL
	 * @throws Exception
	 */
	public SendEMail(
			String token,
			String numberOfRooms,
			String address,
			String price,
			String addressURL,
			String toEMails) throws Exception {
		
		if(new File(token).exists()) {
			System.out.println("Checking token file.");
			BufferedReader bufferedReader = new BufferedReader(
													new InputStreamReader(
															new FileInputStream(token)));
			credentialUser = bufferedReader.readLine(); // Read 1st line.
			credentialSecret = bufferedReader.readLine(); //Read 2nd line.
			bufferedReader.close();
		} else {
			throw new IllegalArgumentException("No such file - " + token);
		}
		
		System.out.println("SSLEmail Start");
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
		props.put("mail.smtp.port", "465"); //SSL Port
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); //SSL Factory Class
		props.put("mail.smtp.auth", "true"); //enable authentication
		props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
		
		//create Authenticator object to pass in Session.getInstance argument
		Authenticator auth = new Authenticator() {
			//override the getPasswordAuthentication method
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(credentialUser, credentialSecret);
			}
		};
		Session session = Session.getInstance(props, auth);
		
		MimeMessage msg = new MimeMessage(session);
		//set message headers
		msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
		msg.addHeader("format", "flowed");
		msg.addHeader("Content-Transfer-Encoding", "8bit");
		 
		msg.setFrom(new InternetAddress("jedihonest@gmail.com", "AlGo"));
		msg.setSubject(numberOfRooms + " tuba - " + address + " - " + price, "UTF-8");
		msg.setText("Link:\n" + addressURL, "UTF-8");
		msg.setSentDate(new Date());
		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEMails, false));
		System.out.println("Message is ready");
  
		Transport.send(msg);  
		System.out.println("EMail Sent Successfully!!");
	}

}
