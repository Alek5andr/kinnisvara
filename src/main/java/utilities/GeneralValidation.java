/**
 * This class provides with methods for different kinds of validation for an element(s).
 */

package utilities;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.BrowserFactory;

/**
 * @author aleksandr.govorkov
 *
 */
public class GeneralValidation {

	private WebDriver driver = BrowserFactory.getFactory().getDriver();
	private int waitTime = 5;
	private final WebDriverWait wait = new WebDriverWait(driver, this.waitTime);


	public String get_Title() {
		String title = driver.getTitle();
		return title;
	}
	
	public boolean getPartOfTitle(String partOfTitle) {
		if (driver.getTitle().contains(partOfTitle)) {
			return true;
		}
		return false;
	}

	public boolean isElementVisible(WebElement webElement) {
		try {
			webElement.isDisplayed();
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public boolean isElementsVisible(List<WebElement> webElements) {
		if (webElements.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isStaleElements(WebDriver driver, By locator) {
		try {
			setImplicitWaitTime();
			driver.findElements(locator);
			return true;
		} catch (StaleElementReferenceException e) {
			return false;
		}
	}
	
	public boolean isStaleElement(WebElement webElement) {
		try {
			webElement.getSize();
			return true;
		} catch (StaleElementReferenceException e) {
			return false;
		}
	}

	public boolean isElementPresent(WebDriver driver, By locator) {
		try {
			setImplicitWaitTime();
			driver.findElement(locator);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public boolean isElementPresent(WebElement webElement) {
		try {
			webElement.getSize();
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public boolean areElementsPresent(WebDriver driver, By locator) {
		try {
			setImplicitWaitTime();
			driver.findElements(locator);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public boolean areElementsPresent(List<WebElement> webElement) {
		if (webElement.isEmpty())
			return false;
		return true;
	}

	public boolean waitForWebElementToDisplay(WebElement webElement) {
		try {
			wait.until(ExpectedConditions.visibilityOf(webElement));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}

	public boolean waitForWebElementsToDisplay(List<WebElement> webElements) {
		try {
			wait.until(ExpectedConditions.visibilityOfAllElements(webElements));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	
	public boolean waitForElementsToBeClickable(List<WebElement> webElements) {
		try {
			wait.until(elementsToBeClickable(webElements));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	
	/**
	   * Implementation is adopted from Selenium - ExpectedConditions class.
	   * An expectation for checking elements are visible and enabled such that you
	   * can click it.
	   *
	   * @param elements the WebElement
	   * @return the (same) WebElements once they are clickable (visible and enabled)
	   */
	  public static ExpectedCondition<List<WebElement>> elementsToBeClickable(
	      final List<WebElement> elements) {
	    return new ExpectedCondition<List<WebElement>>() {

	      public ExpectedCondition<List<WebElement>> visibilityOfElements =
	          ExpectedConditions.visibilityOfAllElements(elements);

	      @Override
	      public List<WebElement> apply(WebDriver driver) {
	        List<WebElement> elements = visibilityOfElements.apply(driver);
	        try {
	          if (elements != null && elements.iterator().next().isEnabled()) {
	            return elements;
	          } else {
	            return null;
	          }
	        } catch (StaleElementReferenceException e) {
	          return null;
	        }
	      }

	      @Override
	      public String toString() {
	        return "Elements to be clickable: " + elements;
	      }
	    };
	  }

	public boolean waitForWebElementToDismiss(WebElement webElement) {
		try {
			wait.until(invisibilityOf(webElement));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}

	// Implementation is adopted from Selenium - ExpectedConditions class
	private ExpectedCondition<Boolean> invisibilityOf(final WebElement element) {
		return new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				try {
					return isElementPresent(element) ? false : true;
				} catch (NoSuchElementException e) {
					// Returns true because the element is not present in DOM.
					// The
					// try block checks if the element is present but is
					// invisible.
					return true;
				} catch (StaleElementReferenceException e) {
					// Returns true because stale element reference implies that
					// element
					// is no longer visible.
					return true;
				}
			}
		};
	}

	public boolean waitForFullyLoadedPage(String title) {
		try {
			wait.until(ExpectedConditions.titleIs(title));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	
	private void setImplicitWaitTime() {
		driver.manage().timeouts().implicitlyWait(this.waitTime, TimeUnit.SECONDS);
	}

}
