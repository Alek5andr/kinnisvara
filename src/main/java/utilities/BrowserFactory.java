/**
 * This class provides with initialization of a chosen browser.
 */

package utilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.remote.UnreachableBrowserException;

/**
 * @author aleksandr.govorkov
 *
 */

public class BrowserFactory {

	private static BrowserFactory factory;
	private WebDriver driver = null;
	private static SessionId session = null;
	private int pageTimeoutInSeconds;

	private final String OPERATING_SYSTEM = System.getProperty("os.name");
	private final String CHROMEDRIVER_LOCATION_WINDOWS = ".\\Drivers\\chromedriver_win32\\chromedriver.exe";
	private final String CHROMEDRIVER_LOCATION_UNIX = "/home/localadmin/Documents/chromedriver_linux64/chromedriver";
	private final String FIREFOXBINARY_LOCATION_WINDOWS = "C:\\Program Files (x86)\\Mozilla Firefox 37\\firefox.exe";
	private final String FIREFOXBINARY_LOCATION_UNIX = "/usr/lib/firefox37/firefox";
	private DesiredCapabilities capabilities = null;
	private final String SELENIUM_GRID_URL_WINDOWS = "http://192.168.218.60:4445/wd/hub"; //CHANGE THIS UN UPLOADING TO ANOTEHR MACHINE
	private final String SELENIUM_GRID_URL_LINUX = "http://192.168.219.74:4445/wd/hub"; //CHANGE THIS UN UPLOADING TO ANOTEHR MACHINE
	private String chromeDriverLocation = "";
	private static final String browserName = "Firefox";

	public WebDriver getDriver() {
		driver.manage().deleteAllCookies();
		return driver;
	}
	
	public SessionId getDriverSessionID() {
		try {
			switch (browserName) {
				default:
					throw new IllegalArgumentException("No such driver implemneted yet - " + browserName);
				case "Firefox":
					session = ((FirefoxDriver)driver).getSessionId();
					break;
				case "IE":
					session = ((InternetExplorerDriver)driver).getSessionId();
					break;
				case "Chrome":
					session = ((ChromeDriver)driver).getSessionId();
					break;					
			}
		} catch (ClassCastException e) {
			session = ((RemoteWebDriver)driver).getSessionId();
		}
		return session;
	}

	public int getPageTimeoutInSeconds() {
		return pageTimeoutInSeconds;
	}

	public static BrowserFactory getFactory() {
		if (factory == null) {
			factory = new BrowserFactory(browserName, 10);
		}
		return factory;
	}

	private BrowserFactory(String browserName, int implicitWaitTimeSeconds) {
		setTimeoutForOperatingSystem();
		System.out.println("\nOperating system is " + OPERATING_SYSTEM);
		System.out.println("Opening browser.");

		switch (browserName) {
			case "Firefox":
				capabilities = DesiredCapabilities.firefox();
				capabilities.setVersion("37");
				capabilities.setCapability("firefox_profile", new FirefoxProfile());
				
				try {
					switch (OPERATING_SYSTEM) {
						case "Windows 10":
							setPlatformToCapabilities(capabilities);
							capabilities.setCapability("firefox_binary", FIREFOXBINARY_LOCATION_WINDOWS);
							initiateRemoteWebDriver(capabilities);
							announceLaunchOfDriver(browserName, "remotely");
							break;
						case "Linux":
							setPlatformToCapabilities(capabilities);
							capabilities.setCapability("firefox_binary", FIREFOXBINARY_LOCATION_UNIX);
							initiateRemoteWebDriver(capabilities);
							announceLaunchOfDriver(browserName, "remotely");
							break;
						default:
							throw new IllegalArgumentException("No such OS is implemented yet - " + OPERATING_SYSTEM);
					}
				} catch (UnreachableBrowserException e) {
					driver = new FirefoxDriver(capabilities);
					announceLaunchOfDriver(browserName, "locally");
				}
				driver.manage().window().maximize();
				break;

			case "IE":
				driver =  new InternetExplorerDriver();
				break;

			case "Chrome":
				/*
				ChromeDriverService service = new ChromeDriverService.Builder()
						.usingDriverExecutable(new File(chromeDriverLocation)).usingPort(4444).build();
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--dns-prefetch-disable"
						, "--start-maximized"
						, "--disable-background-networking"
						, "--disable-component-extensions-with-background-pages");
				driver = new ChromeDriver(service, options);
				*/
				
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--dns-prefetch-disable"
						, "--start-maximized"
						, "--disable-background-networking"
						, "--disable-component-extensions-with-background-pages");
				capabilities = DesiredCapabilities.chrome();
				capabilities.setVersion("56.0");
				
				try {
					switch (OPERATING_SYSTEM) {
						case "Windows 10":
							setPlatformToCapabilities(capabilities);
							capabilities.setCapability(ChromeOptions.CAPABILITY, options);
							initiateRemoteWebDriver(capabilities);
							announceLaunchOfDriver(browserName, "remotely");
							break;
						case "Linux":
							setPlatformToCapabilities(capabilities);
							capabilities.setCapability(ChromeOptions.CAPABILITY, options);
							initiateRemoteWebDriver(capabilities);
							announceLaunchOfDriver(browserName, "remotely");
							break;
						default:
							throw new IllegalArgumentException("No such OS is implemented yet - " + OPERATING_SYSTEM);
					}
				} catch (UnreachableBrowserException e) {
					ChromeDriverService service = new ChromeDriverService.Builder()
							.usingDriverExecutable(new File(chromeDriverLocation)).usingPort(4445).build();
					driver = new ChromeDriver(service, options);
					announceLaunchOfDriver(browserName, "locally");
				}
				break;

			default:
				throw new IllegalArgumentException("Unsupported browser type - " + browserName);
		}
		this.setupImplicitWaitSeconds(implicitWaitTimeSeconds);
	}

	private void setTimeoutForOperatingSystem() {
		switch (OPERATING_SYSTEM) {
			case "Windows 10":
			chromeDriverLocation = CHROMEDRIVER_LOCATION_WINDOWS;
			pageTimeoutInSeconds = 10;
				break;
			case "Linux":
			chromeDriverLocation = CHROMEDRIVER_LOCATION_UNIX;
			pageTimeoutInSeconds = 20;
				break;
			default:
				throw new IllegalArgumentException("Unsupported OS - " + OPERATING_SYSTEM);
		}
	}

	private void setupImplicitWaitSeconds(int implicitWaitSeconds) {
		driver.manage().timeouts().implicitlyWait(implicitWaitSeconds, TimeUnit.SECONDS);
	}
	
	private void setPlatformToCapabilities(DesiredCapabilities capabilities) {
		switch (OPERATING_SYSTEM) {
			case "Windows 10":
				capabilities.setPlatform(Platform.WIN10);
				break;
			case "Linux":
				capabilities.setPlatform(Platform.LINUX);
				break;
			default:
				throw new IllegalArgumentException("No such OS is implemented yet - " + OPERATING_SYSTEM);
		}
	}
	
	private void initiateRemoteWebDriver(DesiredCapabilities capabilities) {
		try {
			switch (OPERATING_SYSTEM) {
				default:
					throw new IllegalArgumentException("No such OS is implemented yet - " + OPERATING_SYSTEM);
				case "Windows 10":
					driver = new RemoteWebDriver(new URL(SELENIUM_GRID_URL_WINDOWS), capabilities);
					break;
				case "Linux":
					driver = new RemoteWebDriver(new URL(SELENIUM_GRID_URL_LINUX), capabilities);
					break;
			}
		} catch (MalformedURLException e) {
			System.out.println("Check your URL. Perhaps, it is invalid.");
		}
		
	}
	
	private void announceLaunchOfDriver(String browserName, String how) {
		System.out.println("Launching '" + browserName + "' " + how + ".");
	}

}
