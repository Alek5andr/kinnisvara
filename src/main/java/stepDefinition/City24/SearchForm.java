/**
 * 
 */

package stepDefinition.City24;

import org.junit.Assert;

import pageObjects.City24.SearchFormPO;
import utilities.GeneralValidation;

/**
 * @author Aleksandr.Govorkov
 *
 */
public class SearchForm extends GeneralValidation {
	private SearchFormPO searchFormObjects = new SearchFormPO();

	public void focusOnSearchField() {
		searchFormObjects.clickSearchField();
	}
	
	public void enterDomainToSearchIn(String cityName) {
		System.out.println("Entering '" + cityName + "' into search field.");
		searchFormObjects.enterCityIntoSearchField(cityName);
	}
	
	public void checkPrivateAds() {
		System.out.println("Checking 'Ainult tavakasutajate kuulutused'.");
		searchFormObjects.checkPrivateAdsCheckbox();
	}
	
	public void enterMinSquareMeters(int number) {
		System.out.println("Entering '" + number + "' to Suuruse vahemik - min.");
		searchFormObjects.enterToMinSquareMetersField(number);
	}
	
	public void enterMaxSquareMeters(int number) {
		System.out.println("Entering '" + number + "' to Suuruse vahemik - max.");
		searchFormObjects.enterToMaxSquareMetersField(number);
	}
	
	public void enterMinRoom(int number) {
		System.out.println("Entering '" + number + "' to Tubade arv - min.");
		searchFormObjects.enterToMinRoomsField(number);
	}
	
	public void enterMaxRoom(int number) {
		System.out.println("Entering '" + number + "' to Tubade arv - max.");
		searchFormObjects.enterToMaxRoomsField(number);
	}
	
	public void enterMinPrice(int number) {
		System.out.println("Entering '" + number + "' to Hinnavahemik - min.");
		searchFormObjects.enterToMinPriceField(number);
	}
	
	public void enterMaxPrice(int number) {
		System.out.println("Entering '" + number + "' to Hinnavahemik - max.");
		searchFormObjects.enterToMaxPriceField(number);
	}
	
	public void enterMinFloor(int number) {
		System.out.println("Entering '" + number + "' to Korruse vahemik - min.");
		searchFormObjects.enterToMinFloorField(number);
	}
	
	public void enterMaxFloor(int number) {
		System.out.println("Entering '" + number + "' to Korruse vahemik - max.");
		searchFormObjects.enterToMaxFloorField(number);
	}
	
	public void expandSearchOptions() {
		System.out.println("Expanding search.");
		searchFormObjects.clickMoreSearchOptionsBtn();
	}
	
	public void waitUntilSearchFieldResults() {
		waitForWebElementsToDisplay(searchFormObjects.getSearchFieldResultSections());
	}
	
	public void checkRegionName(String regionName) {
		System.out.println("Selecting '" + regionName + "' region.");
		searchFormObjects.checkRegion(regionName);
	}
	
	public void assertDismissTag(String dismissTagName) {
		System.out.println("Checking that '" + dismissTagName + "' dismiss tag appeared.");
		Assert.assertTrue("'" + dismissTagName + "' dismiss tag did not appear.", super.isElementPresent(searchFormObjects.getDismissTag(dismissTagName)));
	}
	
	/**
	 * This method is used to select region until its dismiss tag appears.
	 * 
	 * @param dismissTagName - region name in dismiss tag.
	 * @param regionName - region to check.
	 */
	public void checkRegionNameUntilDismissTagAppears(String dismissTagName, String regionName) {
		while (super.waitForWebElementToDisplay(searchFormObjects.getDismissTag(dismissTagName)) == false) {
			checkRegionName(regionName);
		}
		
	}

	public void executeSearch() {
		System.out.println("Executing search.");
		searchFormObjects.clickSearchArrowBtn();
	}
	
	public void selectObjectTypeByName(String objectType) {
		System.out.println("Selecting object type - " + objectType);
		searchFormObjects.selectObjectType(objectType);
	}
	
}
