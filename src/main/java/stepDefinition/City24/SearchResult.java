/**
 * 
 */

package stepDefinition.City24;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

import org.openqa.selenium.StaleElementReferenceException;

import pageObjects.City24.SearchResultPO;
import utilities.GeneralValidation;
import utilities.SendEMail;

/**
 * @author Aleksandr.Govorkov
 *
 */

public class SearchResult extends GeneralValidation {
	private SearchResultPO searchResultObjects = new SearchResultPO();

	private String addressInFile = null;
	private String priceInFile = null;
	private String topFirstFullPrice = null;
	private String topFirstPriceWithoutEUR = null;
	private int topFirstPriceWithoutEURWithoutMiddleSpace = 0;
	private String topFirstAddress = null;
	private String topFirstAddressURL = null;
	private final String OPERATING_SYSTEM = System.getProperty("os.name");
	private String token = ".\\TokenFile\\Token";

	public void sortBy(String criteria) {
		System.out.println("Sorting by '" + criteria + "'.");
		searchResultObjects.selectSortingValue(criteria);
	}

	public void waitUntilSecondSortingDropdownList() {
		try {
			waitForWebElementToDisplay(searchResultObjects.getSecondSortingDropdownList());
		} catch (StaleElementReferenceException e) {
			waitUntilSecondSortingDropdownList();
		}
	}

	public void readPreviouslyFoundAddressNprice(String fileWithPreviouslyFoundAddressNprice) {
		System.out.println("Reading address and price from '" + fileWithPreviouslyFoundAddressNprice + "' file.");
		if ("Linux".equals(OPERATING_SYSTEM)) {
			fileWithPreviouslyFoundAddressNprice = fileWithPreviouslyFoundAddressNprice.replaceAll("\\\\+", "/");
		}
		
		try {
			if (new File(fileWithPreviouslyFoundAddressNprice).exists()) {
				BufferedReader fileWithPreviouslyFoundFlat = new BufferedReader(
						new InputStreamReader(new FileInputStream(fileWithPreviouslyFoundAddressNprice), "UTF-8"));
				// Read FIRST line from the file.
				addressInFile = fileWithPreviouslyFoundFlat.readLine();
				// Read SECOND line, because you stop reading from file at
				// the beginning of second line.
				priceInFile = fileWithPreviouslyFoundFlat.readLine();
				fileWithPreviouslyFoundFlat.close();
			}
		} catch (UnsupportedEncodingException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	public void getTopFirstPrice() {
		System.out.println("Getting top first price.");
		topFirstFullPrice = searchResultObjects.getFirstTopPrice().getText();
		if (topFirstFullPrice.endsWith(" EUR")) {
			// Remove 'EUR' from the price.
			topFirstPriceWithoutEUR = topFirstFullPrice.substring(0, topFirstFullPrice.length() - 4);
			// Remove (big) space between numbers in the price.
			topFirstPriceWithoutEURWithoutMiddleSpace = Integer
					.parseInt(topFirstPriceWithoutEUR.replaceAll("\\s+", ""));
		} else {
			topFirstPriceWithoutEURWithoutMiddleSpace = Integer.parseInt(topFirstFullPrice.replaceAll("\\s+", ""));
		}
	}

	public void getTopFirstAddress() {
		System.out.println("Getting top first address.");
		topFirstAddress = searchResultObjects.getFirstTopAddress().getText();
		topFirstAddressURL = searchResultObjects.getFirstTopAddress().getAttribute("href");
	}

	public void saveNewAddressNprice(String fileWithPreviouslyFoundAddressNprice, int maxPrice, String toEMails) {
		System.out.println("Saving new address and price to '" + fileWithPreviouslyFoundAddressNprice + "' file.");		
		if ( (addressInFile == null) && (topFirstPriceWithoutEURWithoutMiddleSpace <= maxPrice) ) {
			writeDataToFile(fileWithPreviouslyFoundAddressNprice);
			sendEMail(toEMails);
		} else if ( !(addressInFile.equals(topFirstAddress)) && (topFirstPriceWithoutEURWithoutMiddleSpace <= maxPrice) ) {
			writeDataToFile(fileWithPreviouslyFoundAddressNprice);
			sendEMail(toEMails);
		} else if ( (addressInFile.equals(topFirstAddress)) && (topFirstPriceWithoutEURWithoutMiddleSpace <= maxPrice) ) {
			if (Integer.parseInt(priceInFile) != topFirstPriceWithoutEURWithoutMiddleSpace) {
				writeDataToFile(fileWithPreviouslyFoundAddressNprice);
				sendEMail(toEMails);
			}
		}
	}
	
	private void writeDataToFile(String fileWithPreviouslyFoundAddressNprice) {
		if ("Linux".equals(OPERATING_SYSTEM)) {
			fileWithPreviouslyFoundAddressNprice = fileWithPreviouslyFoundAddressNprice.replaceAll("\\\\+", "/");
		}
		
		try {
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(fileWithPreviouslyFoundAddressNprice), "UTF-8"));
			writer.write(topFirstAddress + "\n"); // writer.newLine(); -
												// for Windows only.
			writer.write(Integer.toString(topFirstPriceWithoutEURWithoutMiddleSpace) + "\n");
			writer.write(topFirstAddressURL);
			writer.flush();
			writer.close();
		} catch (IOException e){
			System.out.println(e.getMessage());
		}
	}
	
	private void sendEMail(String toEMails) {
		if ("Linux".equals(OPERATING_SYSTEM)) {
			System.out.println("Checking OS before entering SendEMail class.");
			token = token.replaceAll("\\\\+", "/");
		}
		
		try {
			System.out.println("Before entering SendEMail class.");
			new SendEMail(token, searchResultObjects.getFirstTopNumbersOfRooms().getText(), topFirstAddress, topFirstFullPrice, topFirstAddressURL, toEMails);
			System.out.println("After entering SendEMail class.");
		} catch (Exception e) {
			System.out.println("E-mail was not sent. Error occured.");
			e.printStackTrace();
		}
	}

}
