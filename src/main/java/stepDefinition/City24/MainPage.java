/**
 * 
 */

package stepDefinition.City24;

import org.junit.Assert;

import pageObjects.City24.MainPagePO;
import utilities.GeneralValidation;

/**
 * @author Aleksandr.Govorkov
 *
 */

public class MainPage extends GeneralValidation {
	private MainPagePO mainPageObjects = new MainPagePO();
	
	public void openHomePage() {
		System.out.println("Navigating to 'City 24' home page.");
		mainPageObjects.navigateToHomePage();
	}
	
	public void assertTitle(String title) {
		Assert.assertEquals("It is not '" + title + "'.", title, super.get_Title());
	}
	
	public void dismissCookies() {
		System.out.println("Dismissing cookies.");
		mainPageObjects.clickAcceptCookiesBtn();
	}
	
	public void dismissAdvertisement() {
		System.out.println("Dismissing advertisement.");
		if (isElementPresent(mainPageObjects.getAdvertisementDismissBtn()) == true) {
			while (mainPageObjects.getAdvertisementDismissBtn().isDisplayed() == true) {
				mainPageObjects.clickAdvertisementDismissBtn();
			}
		}
	}

}
