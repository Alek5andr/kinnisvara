/**
 * 
 */

package stepDefinition.KV;

import pageObjects.KV.SearchFormPO;
import utilities.GeneralValidation;

/**
 * @author Aleksandr.Govorkov
 *
 */
public class SearchForm extends GeneralValidation {
	private SearchFormPO searchFormObjects = new SearchFormPO();
	
	public void checkPrivateAds() {
		System.out.println("Checking 'Ainult tavakasutajate kuulutused'.");
		searchFormObjects.checkPrivateAdsCheckbox();
	}
	
	public void enterMinSquareMeters(int number) {
		System.out.println("Entering '" + number + "' to Suuruse vahemik - min.");
		searchFormObjects.enterToMinSquareMetersField(number);
	}
	
	public void enterMaxSquareMeters(int number) {
		System.out.println("Entering '" + number + "' to Suuruse vahemik - max.");
		searchFormObjects.enterToMaxSquareMetersField(number);
	}
	
	public void enterMinRoom(int number) {
		System.out.println("Entering '" + number + "' to Tubade arv - min.");
		searchFormObjects.enterToMinRoomsField(number);
	}
	
	public void enterMaxRoom(int number) {
		System.out.println("Entering '" + number + "' to Tubade arv - max.");
		searchFormObjects.enterToMaxRoomsField(number);
	}
	
	public void enterMinPrice(int number) {
		System.out.println("Entering '" + number + "' to Hinnavahemik - min.");
		searchFormObjects.enterToMinPriceField(number);
	}
	
	public void enterMaxPrice(int number) {
		System.out.println("Entering '" + number + "' to Hinnavahemik - max.");
		searchFormObjects.enterToMaxPriceField(number);
	}
	
	public void enterMinFloor(int number) {
		System.out.println("Entering '" + number + "' to Korruse vahemik - min.");
		searchFormObjects.enterToMinFloorField(number);
	}
	
	public void enterMaxFloor(int number) {
		System.out.println("Entering '" + number + "' to Korruse vahemik - max.");
		searchFormObjects.enterToMaxFloorField(number);
	}
	
	public void expandSearchOptions() {
		System.out.println("Expanding search.");
		searchFormObjects.clickMoreSearchOptionsBtn();
	}
	
	public void waitUntilRegionsCheckboxes() {
		waitForElementsToBeClickable(searchFormObjects.getRegionsCheckboxes());
	}

	public void executeSearch() {
		System.out.println("Executing search.");
		searchFormObjects.clickSearchBtn();
	}
	
	public void checkNotLastFloorCheckbox() {
		System.out.println("Checking not last floor checkbox.");
		searchFormObjects.clickNotLastFloorCheckbox();
	}
	
	public void selectDomain(String domain) {
		System.out.println("Selecting domain '" + domain + "'.");
		searchFormObjects.selectDomainByValue(domain);
	}
	
	public void checkRegion(String regionName) {
		System.out.println("Selecting city '" + regionName + "'.");
		searchFormObjects.selectRegionUntilItIsSelected(regionName);
	}
	
	public void checkObjectType(String objectType) {
		System.out.println("Selecting object type '" + objectType + "'.");
		searchFormObjects.clickObject(objectType);
	}
	
}
