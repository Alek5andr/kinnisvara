/**
 * 
 */

package stepDefinition.KV;

import org.junit.Assert;

import pageObjects.KV.MainPagePO;
import utilities.GeneralValidation;

/**
 * @author Aleksandr.Govorkov
 *
 */

public class MainPage extends GeneralValidation {
	private MainPagePO mainPageObjects = new MainPagePO();
	
	public void openHomePage() {
		System.out.println("Navigating to 'KV' home page.");
		mainPageObjects.navigateToHomePage();
	}
	
	public void assertTitle(String title) {
		System.out.println("Asserting title.");
		Assert.assertEquals(title, super.get_Title());
	}
	
	public void assertPartOfTitle(String partOfTitle) {
		System.out.println("Asserting part of title.");
		Assert.assertTrue("Title does not contain '" + partOfTitle + "'.", super.getPartOfTitle(partOfTitle));
	}
	
	public void dismissCookies() {
		System.out.println("Dismissing cookies.");
		mainPageObjects.clickAcceptCookiesBtn();
	}
	
	public void dismissAdvertisement() {
		System.out.println("Dismissing advertisement.");
		if (isElementPresent(mainPageObjects.getAdvertisementDismissBtn()) == true) {
			while (mainPageObjects.getAdvertisementDismissBtn().isDisplayed() == true) {
				mainPageObjects.clickAdvertisementDismissBtn();
			}
		}
	}
	
	public void navigateToObjectPage(String object) {
		System.out.println("Navigating to object type - " + object);
		mainPageObjects.clickObject(object);
	}

}
