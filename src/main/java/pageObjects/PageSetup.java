/**
 * This class provides initialization of elements on specific PageObject class - web page.
 */

package pageObjects;

import org.openqa.selenium.support.PageFactory;

import utilities.BrowserFactory;

/**
 * @author aleksandr.govorkov
 *
 */

public class PageSetup {
	
	
	public PageSetup() {
		PageFactory.initElements(BrowserFactory.getFactory().getDriver(), this);
	}
}
