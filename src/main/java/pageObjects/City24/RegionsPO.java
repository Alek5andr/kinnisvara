/**
 * 
 */

package pageObjects.City24;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import pageObjects.PageSetup;

/**
 * @author aleksandr.govorkov
 *
 */

public class RegionsPO extends PageSetup {

	@FindBy(how = How.ID, using = "label_location_category_Q105407")
	private WebElement kristiineCheckbox;
	
	@FindBy(how = How.ID, using = "label_location_category_Q105408")
	private WebElement lasnamaeCheckbox;
	
	@FindBy(how = How.ID, using = "label_location_category_Q105409")
	private WebElement mustamaeCheckbox;
	
	public WebElement getKristiineCheckbox() {
		return kristiineCheckbox;
	}
	
	public WebElement getLasnamaeCheckbox() {
		return lasnamaeCheckbox;
	}
	
	public WebElement getMustamaeCheckbox() {
		return mustamaeCheckbox;
	}
	
}
