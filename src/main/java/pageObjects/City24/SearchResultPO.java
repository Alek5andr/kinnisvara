/**
 * 
 */
package pageObjects.City24;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import pageObjects.PageSetup;

/**
 * @author aleksandr.govorkov
 *
 */
public class SearchResultPO extends PageSetup {

	@FindBy(how = How.CSS, using = ".sortOptions>div>select")
	private WebElement firstSortingDropdownList;
	
	@FindBy(how = How.ID, using = "sort_direction_id")
	private WebElement secondSortingDropdownList;
	
	@FindBy(how = How.CSS, using = "ol[start='1'] .price>div")
	private WebElement firstTopPrice;
	
	@FindBy(how = How.CSS, using = "ol[start='1'] .addressLink")
	private WebElement firstTopAddress;

	@FindBy(how = How.CSS, using = "ol[start='1'] ol>li:nth-child(2)>strong")
	private WebElement firstTopNumbersOfRooms;
	
	private final String sortByLatesValue = "LATEST_FIRST";
	
	public void clickSortingDropdownList() {
		firstSortingDropdownList.click();
	}
	
	public void selectSortingValue(String criteria) {
		Select select = new Select(firstSortingDropdownList);
		
		switch (criteria) {
			default:
				throw new IllegalArgumentException("No such criteria - " + criteria);
			case "Lisamise aeg":
				select.selectByValue(sortByLatesValue);
		}
	}
	
	public WebElement getSecondSortingDropdownList() {
		return secondSortingDropdownList;
	}
	
	public WebElement getFirstTopPrice() {
		return firstTopPrice;
	}
	
	public WebElement getFirstTopAddress() {
		return firstTopAddress;
	}
	
	public WebElement getFirstTopNumbersOfRooms() {
		return firstTopNumbersOfRooms;
	}
	
}
