/**
 * 
 */

package pageObjects.City24;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import pageObjects.PageSetup;

/**
 * @author aleksandr.govorkov
 *
 */

public class SearchFormPO extends PageSetup {
	private RegionsPO regionsObjects = new RegionsPO();
	
	@FindBy(how = How.CSS, using = ".search_button_wrap>a")
	private WebElement searchArrowBtn;

	@FindBy(how = How.ID, using = "display_text")
	private WebElement searchField;
	
	@FindBy(how = How.CSS, using = "label[for='onlyPrivateUsers']>span")
	private WebElement privateAdsCheckbox;
	
	@FindBy(how = How.CSS, using = "input[name='sizeRangeSearch:minValue']")
	private WebElement minSquareMetersField;
	
	@FindBy(how = How.CSS, using = "input[name='sizeRangeSearch:maxValue']")
	private WebElement maxSquareMetersField;

	@FindBy(how = How.CSS, using = "input[name='roomRangeSearch:minValue']")
	private WebElement minRoomsField;

	@FindBy(how = How.CSS, using = "input[name='roomRangeSearch:maxValue']")
	private WebElement maxRoomsField;

	@FindBy(how = How.CSS, using = "a[class='genericExpand id_advancedSearch_toggler']>span")
	private WebElement moreSearchOptionsBtn;

	@FindBy(how = How.CSS, using = "input[name='myDiv:floorRangeSearch:minValue']")
	private WebElement minFloorField;

	@FindBy(how = How.CSS, using = "input[name='myDiv:floorRangeSearch:maxValue']")
	private WebElement maxFloorField;

	@FindAll(@FindBy(how = How.CLASS_NAME, using = "tokenCategory"))
	private List<WebElement> searchFieldResultSections;

	@FindBy(how = How.ID, using = "li_label_location_category_Q105407")
	private WebElement kristiineDismissTag;

	@FindBy(how = How.ID, using = "li_label_location_category_Q105408")
	private WebElement lasnamaeDismissTag;

	@FindBy(how = How.ID, using = "li_label_location_category_Q105409")
	private WebElement mustamaeDismissTag;

	@FindBy(how = How.CSS, using = "#priceRangeSearch input[class='textfield sliderField example']:nth-child(1)")
	private WebElement minPriceField;

	@FindBy(how = How.CSS, using = "#priceRangeSearch input[class='textfield sliderField example']:nth-child(2)")
	private WebElement maxPriceField;

	@FindBy(how = How.CLASS_NAME, using = "styledBig")
	private WebElement objectTypeSelector;
	
	public void clickSearchArrowBtn() {
		searchArrowBtn.click();
	}
	
	public void clickSearchField() {
		searchField.click();
	}
	
	public void enterCityIntoSearchField(String cityName) {
		searchField.sendKeys(cityName);
	}
	
	public void checkPrivateAdsCheckbox() {
		privateAdsCheckbox.click();
	}
	
	public void enterToMinSquareMetersField(int number) {
		minSquareMetersField.sendKeys(Integer.toString(number));
	}
	
	public void enterToMaxSquareMetersField(int number) {
		maxSquareMetersField.sendKeys(Integer.toString(number));
	}
	
	public void enterToMinRoomsField(int number) {
		minRoomsField.sendKeys(Integer.toString(number));
	}
	
	public void enterToMaxRoomsField(int number) {
		maxRoomsField.sendKeys(Integer.toString(number));
	}
	
	public void enterToMinPriceField(int number) {
		minPriceField.sendKeys(Integer.toString(number));
	}
	
	public void enterToMaxPriceField(int number) {
		maxPriceField.sendKeys(Integer.toString(number));
	}
	
	public void clickMoreSearchOptionsBtn() {
		moreSearchOptionsBtn.click();
	}
	
	public void enterToMinFloorField(int number) {
		minFloorField.sendKeys(Integer.toString(number));
	}
	
	public void enterToMaxFloorField(int number) {
		maxFloorField.sendKeys(Integer.toString(number));
	}

	public List<WebElement> getSearchFieldResultSections() {
		return searchFieldResultSections;
	}
	
	public void checkRegion(String regionName) {
		switch (regionName) {
			default:
				throw new IllegalArgumentException("Such region is not implemented yet - " + regionName);
			case "Kristiine":
				regionsObjects.getKristiineCheckbox().click();
				break;
			case "Lasnamäe":
				regionsObjects.getLasnamaeCheckbox().click();
				break;
			case "Mustamäe":
				regionsObjects.getMustamaeCheckbox().click();
				break;
		}
	}
	
	public WebElement getDismissTag(String dismissTagName) {
		switch (dismissTagName) {
			default:
				throw new IllegalArgumentException("Such dismiss tag is not implemented yet - " + dismissTagName);
			case "Kristiine":
				return kristiineDismissTag;
			case "Lasnamäe":
				return lasnamaeDismissTag;
			case "Mustamäe":
				return mustamaeDismissTag;
		}
	}
	
	public void selectObjectType(String objectType) {
		Select selector = new Select(objectTypeSelector);
		
		switch (objectType) {
			default:
				throw new IllegalArgumentException("Such object type is not implemented yet - " + objectType);
			case "Korter":
				selector.selectByValue("18");
				break;
			case "Maja":
				selector.selectByValue("8");
				break;
			case "Suvila":
				selector.selectByValue("21");
				break;
		}
	}
	
}
