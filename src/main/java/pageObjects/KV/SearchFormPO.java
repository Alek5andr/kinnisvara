/**
 * 
 */

package pageObjects.KV;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import pageObjects.PageSetup;

/**
 * @author aleksandr.govorkov
 *
 */

public class SearchFormPO extends PageSetup {
	private RegionsPO regionsObjects = new RegionsPO();
	private ObjectType objectTypeObjects = new ObjectType();
	
	@FindBy(how = How.CLASS_NAME, using = "btn-start-search")
	private WebElement searchBtn;
	
	@FindBy(how = How.ID, using = "company_id1")
	private WebElement privateAdsCheckbox;
	
	@FindBy(how = How.ID, using = "area_min")
	private WebElement minSquareMetersField;
	
	@FindBy(how = How.ID, using = "area_max")
	private WebElement maxSquareMetersField;

	@FindBy(how = How.ID, using = "rooms_min")
	private WebElement minRoomsField;

	@FindBy(how = How.ID, using = "rooms_max")
	private WebElement maxRoomsField;

	@FindBy(how = How.ID, using = "floor_min")
	private WebElement minFloorField;

	@FindBy(how = How.ID, using = "floor_max")
	private WebElement maxFloorField;

	@FindBy(how = How.ID, using = "not_last")
	private WebElement notLastFloorCheckbox;

	@FindBy(how = How.ID, using = "price_min")
	private WebElement minPriceField;

	@FindBy(how = How.ID, using = "price_max")
	private WebElement maxPriceField;

	@FindBy(how = How.CSS, using = "button[data-toggle='collapse']")
	private WebElement moreSearchOptionsBtn;

	@FindBy(how = How.ID, using = "county")
	private WebElement citySelector;
	
	@FindAll(@FindBy(how = How.CSS, using = "#cities_list>li"))
	private List<WebElement> regionsCheckboxes;
	
	public void clickSearchBtn() {
		searchBtn.click();
	}
	
	public void checkPrivateAdsCheckbox() {
		privateAdsCheckbox.click();
	}
	
	public void enterToMinSquareMetersField(int number) {
		minSquareMetersField.sendKeys(Integer.toString(number));
	}
	
	public void enterToMaxSquareMetersField(int number) {
		maxSquareMetersField.sendKeys(Integer.toString(number));
	}
	
	public void enterToMinRoomsField(int number) {
		minRoomsField.sendKeys(Integer.toString(number));
	}
	
	public void enterToMaxRoomsField(int number) {
		maxRoomsField.sendKeys(Integer.toString(number));
	}
	
	public void enterToMinPriceField(int number) {
		minPriceField.sendKeys(Integer.toString(number));
	}
	
	public void enterToMaxPriceField(int number) {
		maxPriceField.sendKeys(Integer.toString(number));
	}
	
	public void clickMoreSearchOptionsBtn() {
		moreSearchOptionsBtn.click();
	}
	
	public void enterToMinFloorField(int number) {
		minFloorField.sendKeys(Integer.toString(number));
	}
	
	public void enterToMaxFloorField(int number) {
		maxFloorField.sendKeys(Integer.toString(number));
	}
	
	public void clickNotLastFloorCheckbox() {
		notLastFloorCheckbox.click();
	}
	
	public void selectDomainByValue(String domain) {
		Select domainSelector = new Select(citySelector);
		switch (domain) {
			default:
				throw new IllegalArgumentException("Such domain is not implemented yet - " + domain);
			case "Tallinn":
				domainSelector.selectByValue("100");
				break;
			case "Harjumaa":
				domainSelector.selectByValue("1");
				break;
		}
	}
	
	public List<WebElement> getRegionsCheckboxes() {
		return regionsCheckboxes;
	}
	
	/**
	 * This method is used to check region until it is selected.
	 * 
	 * @param regionName - region name.
	 */
	public void selectRegionUntilItIsSelected(String regionName) {
		switch (regionName) {
			default:
				throw new IllegalArgumentException("No such city is implemented yet - " + regionName);
			case "Kristiine":
				selectElementUntilItSelected(regionsObjects.getKristiineCheckbox());
				break;
			case "Mustamäe":
				selectElementUntilItSelected(regionsObjects.getMustamaeCheckbox());
				break;
			case "Lasnamäe":
				selectElementUntilItSelected(regionsObjects.getLasnamaeCheckbox());
				break;
		}
	}
	
	private void selectElementUntilItSelected(WebElement element) {
		while (!element.isSelected()) {
			element.click();
		}
	}
	
	/**
	 * This method is used to check object type until it is selected.
	 * 
	 * @param objectType - name of object type.
	 */	
	public void clickObject(String objectType) {
		switch (objectType) {
			default:
				throw new IllegalArgumentException("Such object type is not implemented yet - " + objectType);
			case "Maja":
				selectElementUntilItSelected(objectTypeObjects.getHouseCheckbox());
				break;
			case "Suvila":
				selectElementUntilItSelected(objectTypeObjects.getSummerCottageCheckbox());
				break;
		}
	}
	
}
