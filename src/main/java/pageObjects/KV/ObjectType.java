/**
 * 
 */
package pageObjects.KV;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import pageObjects.PageSetup;

/**
 * @author aleksandr.govorkov
 *
 */
public class ObjectType extends PageSetup {
	
	@FindBy(how = How.ID, using = "building_type_13")
	private WebElement houseCheckbox;
	
	@FindBy(how = How.ID, using = "building_type_16")
	private WebElement summerCottageCheckbox;

	public WebElement getHouseCheckbox() {
		return houseCheckbox;
	}

	public WebElement getSummerCottageCheckbox() {
		return summerCottageCheckbox;
	}
	
}
