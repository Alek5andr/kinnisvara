/**
 * 
 */
package pageObjects.KV;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import pageObjects.PageSetup;

/**
 * @author aleksandr.govorkov
 *
 */
public class SearchResultPO extends PageSetup {

	@FindBy(how = How.ID, using = "orderby_web")
	private WebElement sortingDropdownList;
	
	@FindAll(@FindBy(how = How.CLASS_NAME, using = "object-price-value"))
	private List<WebElement> firstTopPrice;
	
	@FindAll(@FindBy(how = How.CSS, using = "h2[class='object-title text-truncate']>a"))
	private List<WebElement> firstTopAddress;

	@FindBy(how = How.CSS, using = ".object-rooms.text-center.hide-on-mobile")
	private WebElement firstTopNumbersOfRooms;

	@FindBy(how = How.CSS, using = "button[data-id='parish']")
	private WebElement tallinnDismissTag;

	@FindBy(how = How.CSS, using = "button[data-id='city_13239']")
	private WebElement kristiineDismissTag;

	@FindBy(how = How.CSS, using = "button[data-id='city_13240']")
	private WebElement lasnamaeDismissTag;

	@FindBy(how = How.CSS, using = "button[data-id='city_13241']")
	private WebElement mustamaeDismissTag;

	@FindBy(how = How.CSS, using = "button[class='icon icon-thin']")
	private WebElement majadeDismissTag; //For 'Maja' and 'Suvila' is the same.

	//Useless. See above.
	@FindBy(how = How.CSS, using = "button[data-id='building_type_16']")
	private WebElement suvilaDismissTag;
	
	private final String sortByNewValue = "cdwl";
	
	public void clickSortingDropdownList() {
		sortingDropdownList.click();
	}
	
	public void selectSortingValue(String criteria) {
		Select select = new Select(sortingDropdownList);
		
		switch (criteria) {
			default:
				throw new IllegalArgumentException("No such criteria - " + criteria);
			case "Alates uuemast":
				select.selectByValue(sortByNewValue);
		}
	}
	
	public List<WebElement> getFirstTopPrice() {
		return firstTopPrice;
	}
	
	public List<WebElement> getFirstTopAddress() {
		return firstTopAddress;
	}
	
	public WebElement getFirstTopNumbersOfRooms() {
		return firstTopNumbersOfRooms;
	}
	
	public WebElement getRegionDismissTag(String regionDismissTagName) {
		switch (regionDismissTagName) {
			default:
				throw new IllegalArgumentException("Such region dismiss tag is not implemented yet - " + regionDismissTagName);
			case "Kristiine":
				return kristiineDismissTag;
			case "Lasnamäe":
				return lasnamaeDismissTag;
			case "Mustamäe":
				return mustamaeDismissTag;
		}
	}
	
	public WebElement getCityDismissTag(String cityDismissTagName) {
		switch (cityDismissTagName) {
			default:
				throw new IllegalArgumentException("Such city dismiss tag is not implemented yet - " + cityDismissTagName);
			case "Tallinn":
				return tallinnDismissTag;
		}
	}
	
	public WebElement getObjectDismissTag(String objectDismissTagName) {
		switch (objectDismissTagName) {
			default:
				throw new IllegalArgumentException("Such city dismiss tag is not implemented yet - " + objectDismissTagName);
			case "Maja":
				return majadeDismissTag;
			case "Suvila":
				return majadeDismissTag;
		}
	}
	
}
