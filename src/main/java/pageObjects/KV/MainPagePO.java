/**
 * 
 */

package pageObjects.KV;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import pageObjects.PageSetup;
import utilities.BrowserFactory;

/**
 * @author Aleksandr.Govorkov
 *
 */

public class MainPagePO extends PageSetup {
	
	@FindBy(how = How.CSS, using = "button[data-dismiss='alert']")
	private WebElement acceptCookiesBtn;
	
	@FindBy(how = How.CSS, using = "div[id$='.cross']")
	private WebElement advertisementDismissBtn;
	
	@FindBy(how = How.CSS, using = "a[href='#majad']")
	private WebElement majadPage;
	
	private WebDriver driver = BrowserFactory.getFactory().getDriver();
	private String url = "http://kinnisvaraportaal-kv-ee.postimees.ee/";

	public void navigateToHomePage() {		
		try
        {
			driver.manage().timeouts().pageLoadTimeout(BrowserFactory.getFactory().getPageTimeoutInSeconds(), TimeUnit.SECONDS);
            driver.get(url);
        }
        catch (TimeoutException e)
        {
            performEscapeKeyPress();
        }
	}
	
	private void performEscapeKeyPress()
    {
        Actions action = new Actions(driver);
        action.sendKeys(Keys.ESCAPE);
        action.build();
        action.perform();
    }

	public WebElement getAcceptCookiesBtn() {
		return acceptCookiesBtn;
	}
	
	public void clickAcceptCookiesBtn() {
		try {
			while (getAcceptCookiesBtn().isDisplayed() == true) {
				acceptCookiesBtn.click();
			}
		} catch (StaleElementReferenceException e) {
			System.out.println("StaleElementReferenceException occurred");
			this.clickAcceptCookiesBtn();
		} catch (NoSuchElementException e) {
			System.out.println("Cookies agreement was not found.");
		}
	}
	
	public WebElement getAdvertisementDismissBtn() {
		return advertisementDismissBtn;
	}
	
	public void clickAdvertisementDismissBtn() {
		advertisementDismissBtn.click();
	}
	
	public void clickObject(String object) {
		switch (object) {
			default:
				throw new IllegalArgumentException("Such object type is not implemented yet - " + object);
			case "Korter":
				break;
			case "Majad":
				majadPage.click();
				break;
			case "Suvila":
				break;
		}
	}

}
