/**
 * 
 */
package testsSetup;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import utilities.BrowserFactory;
import utilities.CaptureScreenshot;

/**
 * @author aleksandr.govorkov
 *
 */
public class TestSetup {

	private static boolean isInitialized;
	static WebDriver driver;
	
	@BeforeMethod
	public void Setup(){
		if (!isInitialized) {
			driver = BrowserFactory.getFactory().getDriver();
			Runtime.getRuntime().addShutdownHook(new Thread() {
				public void run() {
					
					try {
						if (BrowserFactory.getFactory().getDriverSessionID() != null) {
							System.out.println("Closing browser.");
							driver.quit();
						}
					} catch (UnreachableBrowserException e) {
						System.out.println("Browser is closed/killed.");
					}
					/*
					System.out.println("Closing browser.");
					driver.quit();
					*/
				}
			});
			
			isInitialized = true;
		}
		driver.manage().deleteAllCookies();
		System.out.println("========== Start of Test ==========");
	}
	
	

	@AfterMethod
	public void tearDown(ITestResult testResult){
		if (testResult.getStatus() == ITestResult.FAILURE){
			new CaptureScreenshot();
		}
		System.out.println("========== End of Test ==========\n");
	}

}
