package tests.KV;

import org.testng.annotations.Test;

import stepDefinition.KV.MainPage;
import stepDefinition.KV.SearchForm;
import stepDefinition.KV.SearchResult;
import testsSetup.TestSetup;


public class KVThreeRooms extends TestSetup {
	private MainPage mainPage = new MainPage();
	private SearchForm searchForm = new SearchForm();
	private SearchResult searchResult = new SearchResult();
	private final String FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE = ".\\PreviousAddressesNprices\\KV\\KV_Three_Rooms_Address_n_Price.txt";
	private final int MAX_PRICE = 100000;
	private final String city = "Tallinn";
	private final String eMails = "valentinalukash@gmail.com";
	
	@Test
	public void testKVThree() {
		mainPage.openHomePage();
		mainPage.assertTitle("Kinnisvara KV.EE - Kinnisvara pakkumised - korterid majad maad äripinnad. ");
		mainPage.dismissCookies();
		//while ( searchResult.isCityDismissTag(city) == false ) { //No need anymore
		searchForm.enterMaxPrice(MAX_PRICE);
		searchForm.enterMinRoom(3);
		searchForm.selectDomain(city);
		searchForm.waitUntilRegionsCheckboxes();
		searchForm.executeSearch();
		searchResult.waitForFullyLoadedPage("Kinnisvara KV.EE - Kinnisvara pakkumised - korterid majad maad äripinnad. - Müüa korterid ");
		searchResult.sortBy("Alates uuemast");
		//}
		searchResult.readPreviouslyFoundAddressNprice(FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE);
		searchResult.getTopFirstAddress();
		searchResult.getTopFirstPrice();
		searchResult.saveNewAddressNprice(FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE, MAX_PRICE, eMails);
	}
	
}
