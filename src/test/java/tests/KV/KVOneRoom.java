package tests.KV;

import org.testng.annotations.Test;

import stepDefinition.KV.MainPage;
import stepDefinition.KV.SearchForm;
import stepDefinition.KV.SearchResult;
import testsSetup.TestSetup;


public class KVOneRoom extends TestSetup {
	private MainPage mainPage = new MainPage();
	private SearchForm searchForm = new SearchForm();
	private SearchResult searchResult = new SearchResult();
	private final String FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE = ".\\PreviousAddressesNprices\\KV\\KV_One_Room_Address_n_Price.txt";
	private final int MAX_PRICE = 60000;
	private final String city = "Tallinn";
	private final String eMails = "jedihonest@gmail.com, valentinalukash@gmail.com";
	
	@Test
	public void testKVOne() {
		mainPage.openHomePage();
		mainPage.assertTitle("Kinnisvara KV.EE - Kinnisvara pakkumised - korterid majad maad äripinnad. ");
		mainPage.dismissCookies();
		//while (searchResult.isCityDismissTag("Tallinn") == false) { //No need anymore
		searchForm.enterMaxPrice(MAX_PRICE);
		searchForm.enterMaxRoom(1);
		searchForm.selectDomain(city);
		searchForm.waitUntilRegionsCheckboxes();
		searchForm.executeSearch();
		searchResult.waitForFullyLoadedPage("Kinnisvara KV.EE - Kinnisvara pakkumised - korterid majad maad äripinnad. - Müüa korterid ");
		searchResult.sortBy("Alates uuemast");
		//}
		searchResult.readPreviouslyFoundAddressNprice(FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE);
		searchResult.getTopFirstAddress();
		searchResult.getTopFirstPrice();
		searchResult.saveNewAddressNprice(FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE, MAX_PRICE, eMails);
	}
	
}
