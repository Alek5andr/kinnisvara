package tests.KV;

import org.testng.annotations.Test;

import stepDefinition.KV.MainPage;
import stepDefinition.KV.SearchForm;
import stepDefinition.KV.SearchResult;
import testsSetup.TestSetup;


public class KVThreeMineRooms extends TestSetup {
	private MainPage mainPage = new MainPage();
	private SearchForm searchForm = new SearchForm();
	private SearchResult searchResult = new SearchResult();
	private final String FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE = ".\\PreviousAddressesNprices\\KV\\KV_Three_Mine_Rooms_Address_n_Price.txt";
	private final int MAX_PRICE = 120000;
	private final String city = "Tallinn";
	private final String eMails = "jedihonest@gmail.com";
	
	@Test
	public void testKVThree() {
		mainPage.openHomePage();
		mainPage.assertTitle("Kinnisvara KV.EE - Kinnisvara pakkumised - korterid majad maad äripinnad. ");
		mainPage.dismissCookies();
		//while ( (searchResult.isRegionDismissTag("Kristiine") && searchResult.isRegionDismissTag("Mustamäe")) == false ) { //No need anymore
		searchForm.enterMaxPrice(MAX_PRICE);
		searchForm.enterMinRoom(3);
		searchForm.expandSearchOptions();
		searchForm.enterMinSquareMeters(60);
		searchForm.enterMinFloor(2);
		searchForm.checkNotLastFloorCheckbox();
		searchForm.selectDomain(city);
		searchForm.waitUntilRegionsCheckboxes();
		searchForm.checkRegion("Kristiine");
		searchForm.checkRegion("Mustamäe");
		searchForm.executeSearch();
		searchResult.waitForFullyLoadedPage("Kinnisvara KV.EE - Kinnisvara pakkumised - korterid majad maad äripinnad. - Müüa korterid ");
		searchResult.sortBy("Alates uuemast");
		//}
		searchResult.readPreviouslyFoundAddressNprice(FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE);
		searchResult.getTopFirstAddress();
		searchResult.getTopFirstPrice();
		searchResult.saveNewAddressNprice(FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE, MAX_PRICE, eMails);
	}
		
}
