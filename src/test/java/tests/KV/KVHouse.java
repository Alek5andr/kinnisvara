package tests.KV;

import org.testng.annotations.Test;

import stepDefinition.KV.MainPage;
import stepDefinition.KV.SearchForm;
import stepDefinition.KV.SearchResult;
import testsSetup.TestSetup;


public class KVHouse extends TestSetup {
	private MainPage mainPage = new MainPage();
	private SearchForm searchForm = new SearchForm();
	private SearchResult searchResult = new SearchResult();
	private final String FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE = ".\\PreviousAddressesNprices\\KV\\KV_House_Address_n_Price.txt";
	private final String objectPage = "Majad";
	private final String objectType = "Maja";
	private final int MAX_PRICE = 200000;
	private final String domain = "Harjumaa";
	private final String eMails = "valentinalukash@gmail.com";
	
	@Test
	public void testKVHome() {
		mainPage.openHomePage();
		mainPage.assertTitle("Kinnisvara KV.EE - Kinnisvara pakkumised - korterid majad maad äripinnad. ");
		mainPage.dismissCookies();
		mainPage.navigateToObjectPage(objectPage);
		//while (searchResult.isObjectTypeDismissTag(objectType) == false) { //No need anymore
		searchForm.selectDomain(domain);
		searchForm.enterMaxPrice(MAX_PRICE);
		searchForm.checkObjectType(objectType);
		searchForm.executeSearch();
		searchResult.waitForFullyLoadedPage("Kinnisvara KV.EE - Kinnisvara pakkumised - korterid majad maad äripinnad. - Müüa korterid ");
		searchResult.sortBy("Alates uuemast");
		//}
		searchResult.readPreviouslyFoundAddressNprice(FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE);
		searchResult.getTopFirstAddress();
		searchResult.getTopFirstPrice();
		searchResult.saveNewAddressNprice(FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE, MAX_PRICE, eMails);
	}
	
}
