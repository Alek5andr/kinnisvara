package tests.City24;

import org.testng.annotations.Test;
import stepDefinition.City24.MainPage;
import stepDefinition.City24.SearchForm;
import stepDefinition.City24.SearchResult;
import testsSetup.TestSetup;

public class City24ThreeMineRooms extends TestSetup {
	private MainPage mainPage = new MainPage();
	private SearchForm searchForm = new SearchForm();
	private SearchResult searchResult = new SearchResult();
	private final String FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE = ".\\PreviousAddressesNprices\\City24\\City24_Three_Mine_Rooms_Address_n_Price.txt";
	private final int MAX_PRICE = 120000;
	private final String eMails = "jedihonest@gmail.com";
	
	@Test
	public void testCity24ThreeMine() {
		mainPage.openHomePage();
		mainPage.assertTitle("Kinnisvara City24 - korterid majad äripinnad maa ost müük rent üür");
		mainPage.dismissCookies();
		searchResult.waitForFullyLoadedPage("Kinnisvara City24 - korterid majad äripinnad maa ost müük rent üür");
		mainPage.dismissAdvertisement();
		searchForm.enterMinSquareMeters(60);
		searchForm.enterMinRoom(3);
		searchForm.enterMaxPrice(MAX_PRICE);
		searchForm.expandSearchOptions();
		searchForm.enterMinFloor(2);
		searchForm.enterMaxFloor(8);
		searchForm.focusOnSearchField();
		searchForm.enterDomainToSearchIn("Tallinn");
		searchForm.waitUntilSearchFieldResults();
		searchForm.checkRegionNameUntilDismissTagAppears("Kristiine", "Kristiine");
		searchForm.checkRegionNameUntilDismissTagAppears("Mustamäe", "Mustamäe");
		searchForm.executeSearch();
		searchResult.waitForFullyLoadedPage("Otsingutulemused - City 24");
		mainPage.dismissAdvertisement();
		searchResult.sortBy("Lisamise aeg");
		searchResult.waitUntilSecondSortingDropdownList();
		searchResult.readPreviouslyFoundAddressNprice(FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE);
		searchResult.getTopFirstPrice();
		searchResult.getTopFirstAddress();
		searchResult.saveNewAddressNprice(FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE, MAX_PRICE, eMails);
	}
	
}
