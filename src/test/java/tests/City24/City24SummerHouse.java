package tests.City24;

import org.testng.annotations.Test;
import stepDefinition.City24.MainPage;
import stepDefinition.City24.SearchForm;
import stepDefinition.City24.SearchResult;
import testsSetup.TestSetup;

public class City24SummerHouse extends TestSetup {
	private MainPage mainPage = new MainPage();
	private SearchForm searchForm = new SearchForm();
	private SearchResult searchResult = new SearchResult();
	private final String FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE = ".\\PreviousAddressesNprices\\City24\\City24_SummerHouse_Address_n_Price.txt";
	private final String objectType = "Suvila";
	private final int MAX_PRICE = 100000;
	private final String eMails = "valentinalukash@gmail.com";
	private final String domain = "Harjumaa";
	
	@Test
	public void testCity24SummerHouse() {
		mainPage.openHomePage();
		mainPage.assertTitle("Kinnisvara City24 - korterid majad äripinnad maa ost müük rent üür");
		mainPage.dismissCookies();
		searchResult.waitForFullyLoadedPage("Kinnisvara City24 - korterid majad äripinnad maa ost müük rent üür");
		mainPage.dismissAdvertisement();
		searchForm.selectObjectTypeByName(objectType);
		searchForm.enterMaxPrice(MAX_PRICE);
		searchForm.focusOnSearchField();
		searchForm.enterDomainToSearchIn(domain);
		searchForm.waitUntilSearchFieldResults();
		searchForm.executeSearch();
		searchResult.waitForFullyLoadedPage("Otsingutulemused - City 24");
		mainPage.dismissAdvertisement();
		searchResult.sortBy("Lisamise aeg");
		searchResult.waitUntilSecondSortingDropdownList();
		searchResult.readPreviouslyFoundAddressNprice(FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE);
		searchResult.getTopFirstPrice();
		searchResult.getTopFirstAddress();
		searchResult.saveNewAddressNprice(FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE, MAX_PRICE, eMails);
	}

}
