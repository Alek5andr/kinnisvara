package tests.City24;

import org.testng.annotations.Test; //TestNG Runner
import stepDefinition.City24.MainPage;
import stepDefinition.City24.SearchForm;
import stepDefinition.City24.SearchResult;
import testsSetup.TestSetup;

public class City24TwoRooms extends TestSetup {
	private MainPage mainPage = new MainPage();
	private SearchForm searchForm = new SearchForm();
	private SearchResult searchResult = new SearchResult();
	private final String FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE = ".\\PreviousAddressesNprices\\City24\\City24_Two_Rooms_Address_n_Price.txt";
	private final String domain = "Tallinn";
	private final int MAX_PRICE = 75000;
	private final String eMails = "jedihonest@gmail.com, valentinalukash@gmail.com";

	@Test
	public void testCity24Two() {
		mainPage.openHomePage();
		mainPage.assertTitle("Kinnisvara City24 - korterid majad äripinnad maa ost müük rent üür");
		mainPage.dismissCookies();
		searchResult.waitForFullyLoadedPage("Kinnisvara City24 - korterid majad äripinnad maa ost müük rent üür");
		mainPage.dismissAdvertisement();
		searchForm.enterMinRoom(2);
		searchForm.enterMaxRoom(2);
		searchForm.expandSearchOptions();
		searchForm.enterMinFloor(2);
		searchForm.enterMaxFloor(8);
		searchForm.enterMaxPrice(MAX_PRICE);
		searchForm.focusOnSearchField();
		searchForm.enterDomainToSearchIn(domain);
		searchForm.waitUntilSearchFieldResults();
		searchForm.executeSearch();
		searchResult.waitForFullyLoadedPage("Otsingutulemused - City 24");
		mainPage.dismissAdvertisement();
		searchResult.sortBy("Lisamise aeg");
		searchResult.waitUntilSecondSortingDropdownList();
		searchResult.readPreviouslyFoundAddressNprice(FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE);
		searchResult.getTopFirstPrice();
		searchResult.getTopFirstAddress();
		searchResult.saveNewAddressNprice(FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE, MAX_PRICE, eMails);
	}

}
