package tests.City24;

import org.testng.annotations.Test;
import stepDefinition.City24.MainPage;
import stepDefinition.City24.SearchForm;
import stepDefinition.City24.SearchResult;
import testsSetup.TestSetup;

public class City24OneRoom extends TestSetup {
	private MainPage mainPage = new MainPage();
	private SearchForm searchForm = new SearchForm();
	private SearchResult searchResult = new SearchResult();
	private final String FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE = ".\\PreviousAddressesNprices\\City24\\City24_One_Room_Address_n_Price.txt";
	private final String domain = "Tallinn";
	private final int MAX_PRICE = 60000;
	private final String eMails = "jedihonest@gmail.com, valentinalukash@gmail.com";
	
	@Test
	public void testCity24One() {
		mainPage.openHomePage();
		mainPage.assertTitle("Kinnisvara City24 - korterid majad äripinnad maa ost müük rent üür");
		mainPage.dismissCookies();
		searchResult.waitForFullyLoadedPage("Kinnisvara City24 - korterid majad äripinnad maa ost müük rent üür");
		mainPage.dismissAdvertisement();
		searchForm.enterMinSquareMeters(28);
		searchForm.enterMaxRoom(1);
		searchForm.enterMaxPrice(MAX_PRICE);
		searchForm.focusOnSearchField();
		searchForm.enterDomainToSearchIn(domain);
		searchForm.waitUntilSearchFieldResults();
		searchForm.executeSearch();
		searchResult.waitForFullyLoadedPage("Otsingutulemused - City 24");
		mainPage.dismissAdvertisement();
		searchResult.sortBy("Lisamise aeg");
		searchResult.waitUntilSecondSortingDropdownList();
		searchResult.readPreviouslyFoundAddressNprice(FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE);
		searchResult.getTopFirstPrice();
		searchResult.getTopFirstAddress();
		searchResult.saveNewAddressNprice(FILE_WITH_PREVIOUSLY_FOUND_ADDRESS_N_PRICE, MAX_PRICE, eMails);
	}

}
